//@ts-check
//
//  (C) 2018 - Oliver Lenehan
//
//  Client Script - SPX Daily Notices
//
//  CLIENT  - Client Related
//  NOTICES - Notices Related
//  PATH    - Strings for file locations
//
//  Documentation available at http://127.0.0.1/doc/home.spx
//
//  SPX Subjects
//  -SUBJECT    & SCHOOL & FAITH
//  -ACTIVITY   & ROBOTS & DEBATE/PUBSPEAK & MUSIC & CHESS
//  -SPORT      & RUGBY & SOCCER & TENNIS & BASKETBALL & CRICKET
//

const scriptAuthor = "Oliver Lenehan";
const scriptCopyright = "(C) 2018";
const scriptName = "St Pius X Daily Notices";
console.info('%c'+scriptName+" - "+scriptCopyright+" "+scriptAuthor, 'color: #ffdd00; font-weight: bold; background: #003362; padding: 8px 30px; font-size: 30px; border-radius: 10px; margin: 20px');

const currDayElement = document.getElementById("currentDay");
const currDayPrefix = "";
const prevDayButton = document.getElementById("prevDayButton");
const nextDayButton = document.getElementById("nextDayButton");
const dayButtonPrefix = "View ";
// @ts-ignore
const noticeSectionElement = document.getElementById("noticeSection");
// @ts-ignore
const noticePageElement = document.getElementById("noticesPage");
const loginPage = document.getElementById("loginPage");
const loginMessageElement = document.getElementById("loginMessage");
const loginUserBarButton = document.getElementById("loginUserBarButton");

var todayDate = new Date();
todayDate.setHours(0,0,0,0);
var currentDate = todayDate;// Used for formatting the page
var currentYear = Number(getCookie("year"));
var NOTICES_CurrentNotices = [];
var currentIds = [];
// Images for each notice
// @ts-ignore
const NOTICES_ImagePaths = {
    "": "/img/icons/PLACEHOLDER.png",
    "ACTIVITY": "/img/icons/SCHOOL.png",
    "BASKETBALL": "/img/icons/BASKETBALL.png",
    "CHESS": "/img/icons/CHESS.png",
    "DEBATE": "/img/icons/DEBATE.png",
    "FAITH": "/img/icons/FAITH.png",
    "MUSIC": "/img/icons/MUSIC.png",
    "ROBOTS": "/img/icons/ROBOTS.png",
    "RUGBY": "/img/icons/RUGBY.png",
    "SCHOOL": "/img/icons/SCHOOL.png",
    "SOCCER": "/img/icons/SOCCER.png",
    "SUBJECT": "/img/icons/SUBJECT.png",
    "TENNIS": "/img/icons/TENNIS.png",
};

const ONE_DAY = 60000*60*24;

function padNumString(num, size) {
    return ('0' + num).substr(-size); 
}

function encodeEntity(text) {
    return text.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#'+i.charCodeAt(0)+';';
    });
}

function formatDate(date, type="") {
    date = new Date(date);
    const months = { 0: "January", 1: "February", 2: "March", 3: "April", 4: "May", 5: "June", 6: "July", 7: "August", 8: "September", 9: "October", 10: "November", 11: "December" };
    const days = { 0: "Sunday", 1: "Monday", 2: "Tuesday", 3: "Wednesday", 4: "Thursday", 5: "Friday", 6: "Saturday" };
    if (type == "names") {
        return days[date.getDay()]+" "+String(date.getDate())+" "+months[date.getMonth()]; //Alternative 1
    } else if (type == "abbr_full") {
        return days[date.getDay()]+" "+padNumString(String(date.getDate()),2)+"/"+padNumString(String(date.getMonth()+1),2)+"/"+(String(date.getFullYear()).substring(2,4));
    } else if (type == "abbr") {
        return days[date.getDay()].substr(0,3)+" "+padNumString(String(date.getDate()),2)+"."+padNumString(String(date.getMonth()+1),2)+"."+(String(date.getFullYear()).substring(2,4));
    } else {
        return padNumString(String(date.getDate()),2)+"."+padNumString(String(date.getMonth()+1),2)+"."+(String(date.getFullYear()).substring(2,4));
    }
}

function formatMarkup(text="") {
    /*var components = [
        {
            "match": "@i",
            "tags": ["em"]
        },{
            "match": "@b",
            "tags": ["strong"]
        },{
            "match": "@s",
            "tags": ["em","strong"]
        }
    ]*/
    var listenItalic = false;
    var listenBold = false;
    var listenStrong = false;
    var listenHL = false;
    var output = text;
    output = output.replace(/(?=[^\\])`i/g, function(str) {
        if (listenItalic === true) {
            listenItalic = false;
            return "</em>";
        } else {
            listenItalic = true;
            return "<em>";
        }
    });
    output = output.replace(/(?=[^\\])`b/g, function(str) {
        if (listenBold === true) {
            listenBold = false;
            return "</strong>";
        } else {
            listenBold = true;
            return "<strong>";
        }
    });
    output = output.replace(/(?=[^\\])`s/g, function(str) {
        if (listenStrong === true) {
            listenStrong = false;
            return "</strong></em>";
        } else {
            listenStrong = true;
            return "<em><strong>";
        }
    });
    output = output.replace(/(?=[^\\])`h/g, function(str) {
        if (listenStrong === true) {
            listenStrong = false;
            return "</span>";
        } else {
            listenStrong = true;
            return "<span class='noticeHL'>";
        }
    });
    var outputLines = output.split("\n");
    outputLines = outputLines.map(function(value) {
        if (/^(\+|-|\*) /gm.test(value)) {
            return "<li>"+value.substr(2)+"</li>";
        } else if (/^(\+|-|\*)/gm.test(value)) {
            return "<li>"+value.substr(1)+"</li>";
        } else {
            return value+"<br/>";
        }
    });
    output = outputLines.join("");
    return output;
}

function NOTICES_Request(date, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            //console.log("RESP", this.responseText);
            //currentNotices = JSON.parse(this.responseText)["notices"]; //removes the date in the json message and is a array of notices
            //currentDay = new Date(JSON.parse(this.response)["date"]);
            //currentDay = formatDate(currentDay);
            //currDayElement.textContent = "Current - "+currentDay;
            if (currDayElement) {
                currDayElement.textContent = currDayPrefix+formatDate(date, "names");
                prevDayButton.textContent = "🡸 "+dayButtonPrefix+formatDate(date.valueOf() - ONE_DAY, "abbr");
                nextDayButton.textContent = dayButtonPrefix+formatDate(date.valueOf() + ONE_DAY, "abbr")+" 🡺";
            }
            
            NOTICES_CurrentNotices = [];
            JSON.parse(this.responseText).forEach((item) => {
                NOTICES_CurrentNotices.push(item);
            });
            // sort years groups ascending order
            NOTICES_CurrentNotices.forEach((item)=>{
                item.address.sort((a,b)=>{
                    if (Number(a) > Number(b)) {
                        return 1;
                    } else if (Number(a) < Number(b)) {
                        return -1;
                    }
                    return 0;
                });
                //console.log(item.address)
            });
            if (callback) {
                callback();
            }
            //console.log(currentNotices);
        }
    };

    //var date = new Date(); //get current time
    //var clientDate = new Date(date.getTime());// - date.getTimezoneOffset()*60*1000);
    //var clientDate = new Date(new Date().getTime()); // Get current time (machine local)
    var clientDate = new Date(date);
    var jsonDate = clientDate.toJSON();

    currentIds = [];
    NOTICES_CurrentNotices.forEach(function(item) {
        currentIds.push(item.id);
    });

    //console.log(currentIds);

    xhttp.open("POST", "/fetchnotices.spx", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("date="+jsonDate);
    //xhttp.send("date="+jsonDate+"&"+"ids="+JSON.stringify(currentIds));
}

function NOTICES_ContainsYear (year, noticesToSearch) {
    if (year == 0) {
        return NOTICES_CurrentNotices;
    }
    var noticesWithYear = []
    noticesToSearch.forEach((item) => {
        if (item.address.includes(year)) {
            noticesWithYear.push(item);
        }
    });
    return noticesWithYear;
}

function CLIENT_ChangeDate (increment) {
    if (increment == 0) {
        currentDate = new Date(todayDate.valueOf() + increment*ONE_DAY);
        NOTICES_Request(todayDate, ()=>{
            CLIENT_SetYear(currentYear);
        });
    } else {
        currentDate = new Date(currentDate.valueOf() + increment*ONE_DAY);
        NOTICES_Request(currentDate, ()=>{
            CLIENT_SetYear(currentYear);
        });
    }
    return;
}

function CLIENT_LoadNotices (relevant, noticesToLoad) {

    while (noticeSectionElement.firstChild) {
        noticeSectionElement.removeChild(noticeSectionElement.firstChild);
    }
    
    
    if (relevant) {
        var insert = " noticeRelevant";
        noticesToLoad.sort((a,b) => {
            if (a.title > b.title) {
                return 1;
            } else if (a.title < b.title) {
                return -1;
            }
            return 0;
        });
    } else {
        var insert = "";
        noticesToLoad.sort((a,b) => {
            if (a.address.length < b.address.length) {
                return 1;
            }
            if (a.address.length == b.address.length) {
                if (a.title > b.title) {
                    return 1;
                } else if (a.title < b.title) {
                    return -1;
                }
                return 0;
            }
            return 0;
        });
    }
    noticesToLoad.sort((a,b) => {
        if (a.urgent && b.urgent) {
            if (a.title > b.title) {
                return -1;
            } else if (a.title == b.title) {
                return a.title-b.title;
            } else {
                return 1;
            }
        }
        if (a.urgent) {
            return -1;
        }
        if (b.urgent) {
            return 1;
        }
        return 0;
    });

    var urgent = "";
    noticesToLoad.forEach((item) => {
        if (item.urgent == true) {
            //console.log(item.urgent);
            urgent = " noticeUrgent";
        } else {
            urgent = ""
        }
        var container = document.createElement("div");
        container.id = "noticeId_"+item.id;
        container.className = "noticeContainer"+insert+urgent;
        var noticeTitle = document.createElement("div");
        noticeTitle.className = "noticeTitle";
        noticeTitle.innerHTML = formatMarkup(encodeEntity(item.title));
        container.appendChild(noticeTitle);
        if (item.message !== "") {
            var noticeMessage = document.createElement("div");
            noticeMessage.className = "noticeMessage";
            noticeMessage.innerHTML = formatMarkup(encodeEntity(item.message));
            container.appendChild(noticeMessage);
        }
        var noticeAddress = document.createElement("div");
        noticeAddress.className = "noticeAddress";
        noticeAddress.textContent = "Years - "+item.address.join(', ');
        container.appendChild(noticeAddress);
        var noticeMeta = document.createElement("div");
        noticeMeta.className = "noticeAuthor";
        noticeMeta.textContent = "Notice - "+item.author+" , Dates: "+formatDate(new Date(item.begin_date))+" -> "+formatDate(new Date(item.end_date));
        container.appendChild(noticeMeta);
        // /*
        var noticePicture = document.createElement("img");
        noticePicture.className = "noticeImage";
        noticePicture.setAttribute("src", NOTICES_ImagePaths[item.subject || ""]);
        container.appendChild(noticePicture);
        // */
        noticeSectionElement.appendChild(container);
    });
    if (noticesToLoad.length === 0) {
        var container = document.createElement("div");
        container.className = "noticeContainer";
        var noticeTitle = document.createElement("div");
        noticeTitle.className = "noticeTitle";
        noticeTitle.textContent = "No notices for today.";
        container.appendChild(noticeTitle);
        var noticeMessage = document.createElement("div");
        var noticeMeta = document.createElement("div");
        noticeMeta.className = "noticeAuthor";
        noticeMeta.textContent = "System Notice";
        container.appendChild(noticeMeta);
        noticeSectionElement.appendChild(container);
    }
}

function CLIENT_SetYear (year, buttonElement=null) {

    //Also set currently active button
    if (!buttonElement) {
        buttonElement = document.getElementById("year"+year);
    }
    if (buttonElement) {
        var yearButtons = document.body.getElementsByClassName("yearButton");
        for (var button in yearButtons) {
            yearButtons[button].className = "yearButton";
        }
        buttonElement.className = "yearButton current";
    }
    document.cookie = "year="+year+";expires="+new Date(todayDate.valueOf() + ONE_DAY*60).toUTCString().replace("GMT", "UTC")+";path=/";
    //console.log(year);
    switch (year) {
        case 0:
            currentYear = 0;
            CLIENT_LoadNotices(false, NOTICES_ContainsYear(0, NOTICES_CurrentNotices));
            break;
        case 5:
            currentYear = 5;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(5, NOTICES_CurrentNotices));
            break;
        case 6:
            currentYear = 6;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(6, NOTICES_CurrentNotices));
            break;
        case 7:
            currentYear = 7;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(7, NOTICES_CurrentNotices));
            break;
        case 8:
            currentYear = 8;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(8, NOTICES_CurrentNotices));
            break;
        case 9:
            currentYear = 9;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(9, NOTICES_CurrentNotices));
            break;
        case 10:
            currentYear = 10;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(10, NOTICES_CurrentNotices));
            break;
        case 11:
            currentYear = 11;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(11, NOTICES_CurrentNotices));
            break;
        case 12:
            currentYear = 12;
            CLIENT_LoadNotices(true, NOTICES_ContainsYear(12, NOTICES_CurrentNotices));
            break;

    }
}

// From W3Schools JS Cookies Example
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}



function CLIENT_ShowLoginForm() {
    if (loginPage.className.includes("hide")) {
        loginPage.className = "";
        loginUserBarButton.textContent = "Hide";
        loginFormElement["username"].focus();
    } else {
        loginPage.className = "hide";
        loginMessageElement.hidden = true;
        loginUserBarButton.textContent = "Login";
    }
}

var loginFormElement = document.getElementById("loginForm");
if (loginFormElement["username"].value == "") {
    loginFormElement["username"].setCustomValidity('Username Required.');
}
loginFormElement["username"].addEventListener("input", function (event) {
    loginMessageElement.hidden = true;
    if (loginFormElement["username"].value == "") {
        loginFormElement["username"].setCustomValidity('Username Required.');
    } else {
        loginFormElement["username"].setCustomValidity('');
    }
});
loginFormElement["password"].addEventListener("input", function (event) {loginMessageElement.hidden = true;});
loginFormElement.addEventListener("submit", function (event) {
    event.preventDefault();
    CLIENT_ValidateLogIn(loginFormElement);
});

function CLIENT_ValidateLogIn(frm) {
    
    const loginForm = frm;
    //loginMessageElement.hidden = true;
    if (loginForm["username"].value == "") {
        loginMessageElement.textContent = "Username Required.";
        loginMessageElement.hidden = false;
    }

    if (loginPage.getElementsByClassName("loader").length < 1) {
        var loader = document.createElement("div");
        loader.className = "loader";
        loginPage.appendChild(loader);
    } else {
        //var loaderElement = loginPage.getElementsByClassName("loader")[0]
        //loaderElement.parentElement.removeChild(loaderElement);
    }

    var oldUser = loginForm["username"].value;
    var user = encodeURIComponent(clientEncodeHash(loginForm["username"].value));
    var pass = encodeURIComponent(clientEncodeHash(loginForm["password"].value));
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // @ts-ignore
            loginFormElement.username.value = decodeURIComponent(user);
            // @ts-ignore
            loginFormElement.password.value = decodeURIComponent(pass);
            // @ts-ignore
            loginFormElement.submit();
            // @ts-ignore
            loginFormElement.username.value = oldUser;
            // @ts-ignore
            loginFormElement.password.value = "";
        } else if (this.readyState == 4) {
            var loaderElement = loginPage.getElementsByClassName("loader")[0]
            loaderElement.parentElement.removeChild(loaderElement);
            loginMessageElement.textContent = "Invalid Username or Password.";
            loginMessageElement.hidden = false;
        }
    };
    xhttp.open("POST", "/edit.spx", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(`username=${user}&password=${pass}&checker=1`);
    
    //console.log("HUI")
    //return true; // send if correctly validated
}

function clientEncodeHash(stringToEncode="") {
    var encodedString = "";

    //ENCODING BASED ON PROGCOMP CIPHER CHALLENGE

    function inRange(num=0) {
        if (num > 126) return 31+(num-126);
        if (num < 32)  return 127-(32-num);
        return num;
    }

    const cipherConstant = 21;
    var cipherShiftBegin = 42;
    var cipherShift = cipherShiftBegin;
    var cipherBuffer = [];
    var count = 0;
    var incrementRunLength = -1;
    var resetNumber = -1;

    for (var char of stringToEncode) {
        if (count === incrementRunLength) {
            cipherShift = resetNumber
            cipherBuffer.shift();
            cipherBuffer.shift();
            count = 0;
        }
        encodedString += String.fromCharCode(inRange(char.charCodeAt(0)+cipherShift));
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);

        incrementRunLength = cipherBuffer[0];
        resetNumber = cipherBuffer[1];
        cipherShift = (cipherShift+1)%95;
        count += 1;
    }

    return encodedString;
}




// Initialise Notices Page
NOTICES_Request(currentDate, ()=> {
    CLIENT_SetYear(currentYear);
    // set todays date for title bar
    var barTitle = document.getElementById("userMenuBar").getElementsByClassName("menuBarTitle")[0];
    barTitle.textContent = "Today's Date: "
    var dateWithEmphasis = document.createElement("em");
    dateWithEmphasis.textContent = formatDate(todayDate, "abbr_full");
    barTitle.appendChild(dateWithEmphasis);
});










