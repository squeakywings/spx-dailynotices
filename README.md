# SPX-DailyNotices
St Pius X College Daily Notices
Node.JS Application


# How to use this package
1. Download the ZIP and unzip it
2. Run the BATCH script or open `server_backend.js` using Node.JS
3. Go to localhost, and view the site


# Proposals
+ Special markup language (bold/italics)
+ Students proposing announcements (tied to name and details to prevent silly submissions)


# Changelog
### v0.2.2
+ Modified markup instructions and fixed bullet points not displaying bug.
### v0.2.1
+ Added new instructions for markup
### v0.2.0
+ Added support for basic markup features (highlight,bold,italic,strong,list)
+ HTML parsing and XSS Prevention
### v0.1.3
+ Git files update/use
### v0.1.2
+ Newline chars available for creating and viewing notices.
### v0.1.1
+ Bug fixes to database and UI
### v0.1.0
+ First Release
