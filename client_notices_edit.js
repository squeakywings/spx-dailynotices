//@ts-check
//
//  (C) 2018 - Oliver Lenehan
//
//  Client Editing Notices Page Script - SPX Daily Notices
//
//  Documentation available at http://127.0.0.1/doc/home.spx
//

const e_scriptAuthor = "Oliver Lenehan";
const e_scriptCopyright = "(C) 2018";
const e_scriptName = "St Pius X Daily Notices Editing Script";
console.info('%c'+e_scriptName+" - "+e_scriptCopyright+" "+e_scriptAuthor, 'color: #ffdd00; font-weight: bold; background: #003362; padding: 8px 30px; font-size: 30px; border-radius: 10px; margin: 20px');

var form = document.forms["editNotice"];

const editNoticePageElement = document.getElementById("editNoticePage");
setCurrForm(form);
// @ts-ignore
const noticeSectionElement = document.getElementById("noticeSection");
// @ts-ignore
const noticePageElement = document.getElementById("noticesPage");
var NOTICES_CurrentNotices = [];
// Images for each notice
// @ts-ignore
const NOTICES_ImagePaths = {
    "": "/img/icons/PLACEHOLDER.png",
    "ACTIVITY": "/img/icons/SCHOOL.png",
    "BASKETBALL": "/img/icons/BASKETBALL.png",
    "CHESS": "/img/icons/CHESS.png",
    "DEBATE": "/img/icons/DEBATE.png",
    "FAITH": "/img/icons/FAITH.png",
    "MUSIC": "/img/icons/MUSIC.png",
    "ROBOTS": "/img/icons/ROBOTS.png",
    "RUGBY": "/img/icons/RUGBY.png",
    "SCHOOL": "/img/icons/SCHOOL.png",
    "SOCCER": "/img/icons/SOCCER.png",
    "SUBJECT": "/img/icons/SUBJECT.png",
    "TENNIS": "/img/icons/TENNIS.png",
};

function padNumString(num, size) {
    return ('0' + num).substr(-size); 
}
function createHtmlFormDate(date) {
    return String(date.getFullYear()+"-"+padNumString(+maxDate.getMonth()+1,2)+"-"+padNumString(maxDate.getDate(),2));
}

// Setup form here and default values
var defaultUsername = decodeURIComponent(getCookie("author")) || "ERROR TEACHER"; //SETUP WITH LOGON
var minDate = new Date();
// HTML FORM style YYYY-MM-DD .. also happens to be todays date
var formatMinDate = minDate.getFullYear()+"-"+padNumString(+minDate.getMonth()+1,2)+"-"+padNumString(minDate.getDate(),2); 
var maxDate = new Date(new Date().valueOf() + 1000*60*60*24*300); // max date to add is 300 days in future... 1000 ms -> 1 min -> 1 hour -> 1 day -> 300 days
var formatMaxDate = maxDate.getFullYear()+"-"+padNumString(+maxDate.getMonth()+1,2)+"-"+padNumString(maxDate.getDate(),2);

var customNoticeEdit = `
	<form id="editNotice" name="editNotice" method="POST" onsubmit="setCurrForm(this);">
	<div class="editNoticeContainer">
		<input type="hidden" name="id" value="6"/>
		<input class="editNoticeTitle" type="text" name="title" value="" required autocomplete="off" placeholder="Notice Title" maxlength="128"><br>
		<textarea class="editNoticeMessage" name="message" placeholder="Notice Content" maxlength="2048"></textarea><br>
		<label id="yearAllCheckbox" class="yearGroupCheckbox"><input type="checkbox" onclick="NOTICES_AllYears();" checked tabindex="-1"/>All Years<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year5" value="true" onclick="NOTICES_AllYears(5);" checked tabindex="-1"/>Year 5<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year6" value="true" onclick="NOTICES_AllYears(6);" checked tabindex="-1"/>Year 6<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year7" value="true" onclick="NOTICES_AllYears(7);" checked tabindex="-1"/>Year 7<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year8" value="true" onclick="NOTICES_AllYears(8);" checked tabindex="-1"/>Year 8<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year9" value="true" onclick="NOTICES_AllYears(9);" checked tabindex="-1"/>Year 9<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year10" value="true" onclick="NOTICES_AllYears(10);" checked tabindex="-1"/>Year 10<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year11" value="true" onclick="NOTICES_AllYears(11);" checked tabindex="-1"/>Year 11<span class="checkmark"></span></label>
		<label class="yearGroupCheckbox"><input type="checkbox" name="year12" value="true" onclick="NOTICES_AllYears(12);" checked tabindex="-1"/>Year 12<span class="checkmark"></span></label>
		
		<input class="editNoticeAuthor" type="text" name="author" value="" required="" placeholder="Announcer">
		<select class="editNoticeImage" name="subject" disabled>
			<option value="ACTIVITY">Activities</option>
			<option value="BASKETBALL">Basketball</option>
			<option value="CHESS">Chess</option>
			<option value="DEBATE">Debating</option>
			<option value="FAITH">Faith</option>
			<option value="SOCCER">Football</option>
			<option value="MUSIC">Music</option>
			<option value="ROBOTS">Robots</option>
			<option value="RUGBY">Rugby</option>
			<option value="SCHOOL">School</option>
			<option value="SUBJECT">Subjects</option>
			<option value="TENNIS">Tennis</option>
		</select>
		<label class="editNoticeDate">From:<input type="date" name="start_date" min="${formatMinDate}" max="${formatMaxDate}" required value="${formatMinDate}" /></label>
		<label class="editNoticeDate eND">To:<input type="date" name="end_date" min="${formatMinDate}" max="${formatMaxDate}" required value="${formatMinDate}" /></label>
		<label class="yearGroupCheckbox" style="margin: 0 0 0 5px;"><input type="checkbox" name="urgent" value="true" tabindex="-1"/>Urgent<span class="checkmark"></span></label>
		<br>
		
		<div class="buttonPanel">
			<button id="newNoticeSubmit" type="submit" class="squareButton buttonSubmit" onclick="NOTICES_RequestAll(()=>{NOTICES_LoadPage();}); ">💾</button>
			<button type="button" class="squareButton buttonCancel" onclick="NOTICES_Cancel();">✖</button>
		</div>
	</div>
	</form>`;

customNoticeEdit = String(customNoticeEdit);

function setCurrForm(frm) {
	form = frm;
	form.onsubmit = (event) => {
		event.preventDefault();
		CLIENT_SubmitChanges(form, (code)=>{
			//location.reload();
			if (code == 200) {
				document.getElementById("newNoticeSubmit").className = "squareButton buttonSubmit";

				var successMessageThere = document.getElementById("successMessage");
				var errorMessageThere = document.getElementById("errorMessage");
				if (errorMessageThere) {
					errorMessageThere.parentNode.removeChild(errorMessageThere);
				}

				var successMessage = document.createElement("h3");
				successMessage.id = "successMessage";
				successMessage.textContent = "Notice Successfully Sent!";
				var noticePageDoc = document.getElementById("editNoticePage");
				if (!successMessageThere) noticePageDoc.appendChild(successMessage);
                NOTICES_Cancel();

			} else if (code == 400) {
				document.getElementById("newNoticeSubmit").className = "squareButton buttonSubmit errored";

				var errorMessageThere = document.getElementById("errorMessage");
				var successMessageThere = document.getElementById("successMessage");
				if (successMessageThere) {
					successMessageThere.parentNode.removeChild(successMessageThere);
				}

				var errorMessage = document.createElement("h3");
				errorMessage.id = "errorMessage";
				errorMessage.textContent = "ERROR"+code+"> Failed to send. At least 1 year group required."
				var noticePageDoc = document.getElementById("editNoticePage");
				if (!errorMessageThere) noticePageDoc.appendChild(errorMessage);
			} else {
				document.getElementById("newNoticeSubmit").className = "squareButton buttonSubmit errored";

				var errorMessageThere = document.getElementById("errorMessage");
				var successMessageThere = document.getElementById("successMessage");
				if (successMessageThere) {
					successMessageThere.parentNode.removeChild(successMessageThere);
				}

				var errorMessage = document.createElement("h3");
				errorMessage.id = "errorMessage";
				errorMessage.textContent = "ERROR"+code+"> Some error occured. Apologies for vagueness."
				var noticePageDoc = document.getElementById("editNoticePage");
				if (!errorMessageThere) noticePageDoc.appendChild(errorMessage);
			}
		});
	};
}

function encodeEntity(text) {
    return text.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#'+i.charCodeAt(0)+';';
    });
}

function formatMarkup(text="") {
    /*var components = [
        {
            "match": "@i",
            "tags": ["em"]
        },{
            "match": "@b",
            "tags": ["strong"]
        },{
            "match": "@s",
            "tags": ["em","strong"]
        }
    ]*/
    var listenItalic = false;
    var listenBold = false;
    var listenStrong = false;
    var output = text;
    output = output.replace(/(?=[^\\])`i/g, function(str) {
        if (listenItalic === true) {
            listenItalic = false;
            return "</em>";
        } else {
            listenItalic = true;
            return "<em>";
        }
    });
    output = output.replace(/(?=[^\\])`b/g, function(str) {
        if (listenBold === true) {
            listenBold = false;
            return "</strong>";
        } else {
            listenBold = true;
            return "<strong>";
        }
    });
    output = output.replace(/(?=[^\\])`s/g, function(str) {
        if (listenStrong === true) {
            listenStrong = false;
            return "</strong></em>";
        } else {
            listenStrong = true;
            return "<em><strong>";
        }
    });
    output = output.replace(/(?=[^\\])`h/g, function(str) {
        if (listenStrong === true) {
            listenStrong = false;
            return "</span>";
        } else {
            listenStrong = true;
            return "<span class='noticeHL'>";
        }
    });
    var outputLines = output.split("\n");
    outputLines = outputLines.map(function(value) {
        if (/^(\+|-|\*) /gm.test(value)) {
            return "<li>"+value.substr(2)+"</li>";
        } else if (/^(\+|-|\*)/gm.test(value)) {
            return "<li>"+value.substr(1)+"</li>";
        } else {
            return value+"<br/>";
        }
    });
    output = outputLines.join("");
    return output;
}

function CLIENT_LogOut() {
	window.location.href = "/";
}

function CLIENT_SubmitChanges(frm, callback) {
	var data = serialize(frm);
	console.log(data)
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			//console.log(this.responseText);
			if (callback) {
                callback(200);
                NOTICES_RequestAll(()=> {
                    NOTICES_LoadPage();
                });
			}
		} else if (this.readyState == 4 && this.status != 200) {
			if (callback) callback(this.status);
		}
	};

	xhttp.open("POST", "/editnotices.spx", true); //send notice, server sees if it exists, if not then it adds it to the database
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(data);
}

function CLIENT_DeleteNotice(noticeElement) {
    var noticeID = noticeElement.id.split("_")[1];
    var confirmed = false;
    if (confirm("Are you sure you want to delete this notice?")) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                NOTICES_RequestAll(()=> {
                    NOTICES_LoadPage();
                });
            }
        };
        xhttp.open("POST", "/deletenotices.spx", true); //send notice, server sees if it exists, if not then it adds it to the database
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("noticeid="+noticeID);
    }
}

function assignHtmlFromString(ele, htmlString) {
	ele.innerHTML = htmlString;
}

function NOTICES_AddNotice() {
	editNoticePageElement.className = "noticesPage editNoticeShown";
	assignHtmlFromString(editNoticePageElement, customNoticeEdit);
	var form = document.forms["editNotice"];
	setCurrForm(form);
    form.id.value = -1;
    form.author.value = defaultUsername;
}

function NOTICES_EditExisting(noticeElement) {
    window.scrollTo(0,0);
    editNoticePageElement.className = "noticesPage editNoticeShown";
	assignHtmlFromString(editNoticePageElement, customNoticeEdit);
	var form = document.forms["editNotice"];
    setCurrForm(form);
    var currNoticeId = Number(noticeElement.id.split("_")[1])
    form.id.value = currNoticeId;
    var noticeObj = {};
    NOTICES_CurrentNotices.forEach(function (item) {
        if (item.id == currNoticeId) {
            noticeObj = item;
        }
    });
    form.title.value = noticeObj.title;
    form.message.value = noticeObj.message;
    var address = noticeObj.address;
    [5,6,7,8,9,10,11,12].forEach(function (item) {
        if (address.includes(item)) {
            form["year"+item].checked = true;
        } else {
            form["year"+item].checked = false;
        }
	});
	if (address == [5,6,7,8,9,10,11,12]) {
		form[3].checked = true;//should be the number of the all years input
	} else {
		form[3].checked = false; //should be the number of the all years input
	}
    form.author.value = noticeObj.author;
    form.subject.value = noticeObj.subject;
    form.urgent.checked = noticeObj.urgent;
    form.start_date.valueAsDate = new Date(noticeObj.begin_date);
    form.end_date.valueAsDate = new Date(noticeObj.end_date);
}

function NOTICES_Cancel() {
	editNoticePageElement.className = "noticesPage editNoticeHidden";
}

var checkYears = [5,6,7,8,9,10,11,12]; //defaults
// sets the buttons toggled by all years button
// for editing dialog
function NOTICES_AllYears(yearGroup=0) {
	var checkBox = document.getElementById("yearAllCheckbox").firstElementChild;
	if (yearGroup == 0) {
		// @ts-ignore
		if (checkBox.checked) {
			for (var i=5; i<13; i++) {
				// @ts-ignore
				document.getElementsByName(`year${i}`)[0].checked = true;
				checkYears.push(i);
			}
		} else {
			for (var i=5; i<13; i++) {
				// @ts-ignore
				document.getElementsByName(`year${i}`)[0].checked = false;
			}
			checkYears = [];
		}
	} else {
		if (checkYears.includes(yearGroup)) {
			var index = checkYears.indexOf(yearGroup);
			checkYears.splice(index, 1);
		} else {
			checkYears.push(yearGroup);
		}
	}
	if (checkYears.length < 8) {
		// @ts-ignore
		checkBox.checked = false;
	} else {
		// @ts-ignore
		checkBox.checked = true;
	}
}

function NOTICES_LoadPage(){
  	while (noticeSectionElement.firstChild) {
    	noticeSectionElement.removeChild(noticeSectionElement.firstChild);
  	}
	NOTICES_CurrentNotices.sort((a,b) => {
		if (a.title > b.title) {
			return 1;
		} else if (a.title < b.title) {
			return -1;
		}
		return 0;
	});
    NOTICES_CurrentNotices.sort((a,b) => {
        if (a.urgent && b.urgent) {
            if (a.title > b.title) {
                return -1;
            } else if (a.title == b.title) {
                return a.title-b.title;
            } else {
                return 1;
            }
        }
        if (a.urgent) {
            return -1;
        }
        if (b.urgent) {
            return 1;
        }
        return 0;
    });

    var urgent = "";
    NOTICES_CurrentNotices.forEach((item) => {
        if (item.urgent == true) {
            //console.log(item.urgent);
            urgent = " noticeUrgent";
        } else {
            urgent = ""
        }
        var container = document.createElement("div");
        container.id = "noticeId_"+item.id;
        container.className = "noticeContainer"+urgent;
        var noticeTitle = document.createElement("div");
        noticeTitle.className = "noticeTitle";
        noticeTitle.innerHTML = formatMarkup(encodeEntity(item.title));
        container.appendChild(noticeTitle);
        if (item.message !== "") {
            var noticeMessage = document.createElement("div");
            noticeMessage.className = "noticeMessage";
            noticeMessage.innerHTML = formatMarkup(encodeEntity(item.message));
            container.appendChild(noticeMessage);
        }
        var noticeAddress = document.createElement("div");
        noticeAddress.className = "noticeAddress";
        noticeAddress.textContent = "Years - "+item.address.join(', ');
        container.appendChild(noticeAddress);
        var noticeAuthor = document.createElement("div");
        noticeAuthor.className = "noticeAuthor";
        noticeAuthor.textContent = "Notice - "+item.author;
        container.appendChild(noticeAuthor);
		var noticeMetaA = document.createElement("div");
		noticeMetaA.className = "noticeAuthor";
		noticeMetaA.textContent = "Begin - "+createHtmlFormDate(new Date(item.begin_date));
        container.appendChild(noticeMetaA);
		var noticeMetaB = document.createElement("div");
		noticeMetaB.className = "noticeAuthor";
		noticeMetaB.textContent = " End - "+createHtmlFormDate(new Date(item.end_date));
        container.appendChild(noticeMetaB);
        // /*
        var noticePicture = document.createElement("img");
        noticePicture.className = "noticeImage";
        noticePicture.setAttribute("src", NOTICES_ImagePaths[item.subject || ""]);
        container.appendChild(noticePicture);
        // */
        var buttonSectionHtml = `
			<button class="squareButton buttonEdit" onclick="NOTICES_EditExisting(this.parentElement.parentElement);">✎</button>
            <button class="squareButton buttonDelete" onclick="CLIENT_DeleteNotice(this.parentElement.parentElement);">🗑</button>`
        var buttonSectionPanel = document.createElement("div");
        buttonSectionPanel.className = "buttonPanel";
        assignHtmlFromString(buttonSectionPanel, buttonSectionHtml);
        container.appendChild(buttonSectionPanel);

        noticeSectionElement.appendChild(container);
	});
	
    if (NOTICES_CurrentNotices.length === 0) {
        var container = document.createElement("div");
        container.className = "noticeContainer";
        var noticeTitle = document.createElement("div");
        noticeTitle.className = "noticeTitle";
        noticeTitle.textContent = "No notices for today.";
        container.appendChild(noticeTitle);
        var noticeMessage = document.createElement("div");
        var noticeMeta = document.createElement("div");
        noticeMeta.className = "noticeAuthor";
        noticeMeta.textContent = "System Notice";
        container.appendChild(noticeMeta);
        noticeSectionElement.appendChild(container);
    }
}

function NOTICES_RequestAll(callback) {
	var date = new Date();
	date.setHours(0,0,0,0);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {					
            NOTICES_CurrentNotices = [];
            JSON.parse(this.responseText).forEach((item) => {
                    NOTICES_CurrentNotices.push(item);
            });
            NOTICES_CurrentNotices.forEach((item)=>{
                item.address.sort((a,b)=>{
                    if (Number(a) > Number(b)) {
                        return 1;
                    } else if (Number(a) < Number(b)) {
                        return -1;
                    }
                    return 0;
                });
                //console.log(item.address)
            });
            if (callback) {
                    callback();
            }
        }
	};

	var clientDate = new Date(date);
	var jsonDate = clientDate.toJSON();

	xhttp.open("POST", "/fetchnotices.spx", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("date="+jsonDate+"&all=true");
}

function encodeHash(stringToEncode="") {
    var encodedString = "";

    //ENCODING BASED ON PROGCOMP CIPHER CHALLENGE

    function inRange(num=0) {
        if (num > 126) return 31+(num-126);
        if (num < 32)  return 127-(32-num);
        return num;
    }

    const cipherConstant = 21;
    var cipherShiftBegin = 42;
    var cipherShift = cipherShiftBegin;
    var cipherBuffer = [];
    var count = 0;
    var incrementRunLength = -1;
    var resetNumber = -1;

    for (var char of stringToEncode) {
        if (count === incrementRunLength) {
            cipherShift = resetNumber
            cipherBuffer.shift();
            cipherBuffer.shift();
            count = 0;
        }
        encodedString += String.fromCharCode(inRange(char.charCodeAt(0)+cipherShift));
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);

        incrementRunLength = cipherBuffer[0];
        resetNumber = cipherBuffer[1];
        cipherShift = (cipherShift+1)%95;
        count += 1;
    }

    return encodedString;
}

NOTICES_RequestAll(()=> {
	NOTICES_LoadPage();
	var loggedInHeading = document.getElementById("userMenuBar").getElementsByClassName("menuBarTitle")[0];
	loggedInHeading.textContent = "";
	var userHeading = document.createElement("em");
	userHeading.textContent = defaultUsername;
	loggedInHeading.appendChild(userHeading);
});















// From W3Schools JS Cookies Example
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Code From - https://code.google.com/archive/p/form-serialize/ All rights to original creator
function serialize(form) {
	if (!form || form.nodeName !== "FORM") {
		return;
	}
	var i, j, q = [];
	for (i = form.elements.length - 1; i >= 0; i = i - 1) {
		if (form.elements[i].name === "") {
			continue;
		}
		switch (form.elements[i].nodeName) {
		case 'INPUT':
			switch (form.elements[i].type) {
			case 'text':
			case 'hidden':
			case 'password':
			case 'button':
			case 'reset':
			case 'date':
			case 'submit':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				break;
			case 'checkbox':
			case 'radio':
				if (form.elements[i].checked) {
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				}						
				break;
			case 'file':
				break;
			}
			break;			 
		case 'TEXTAREA':
			q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
			break;
		case 'SELECT':
			switch (form.elements[i].type) {
			case 'select-one':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				break;
			case 'select-multiple':
				for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
					if (form.elements[i].options[j].selected) {
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
					}
				}
				break;
			}
			break;
		case 'BUTTON':
			switch (form.elements[i].type) {
			case 'reset':
			case 'submit':
			case 'button':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				break;
			}
			break;
		}
	}
	return q.join("&");
}
